FactoryGirl.define do
  factory :question do
    content "How many vowels are there in the English alphabet?"
    answer "five"

    factory :invalid_question do
      content nil
    end
  end
end
