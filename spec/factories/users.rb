FactoryGirl.define do
  factory :user do
    name "Steve Jobs"
    score 1

    factory :invalid_user do
      name nil
    end
  end
end
