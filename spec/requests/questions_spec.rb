require "rails_helper"

RSpec.describe "Questions", type: :request do
  describe "GET /api/questions" do
    it "returns all the questions" do
      create :question, content: "q1", answer: "1"
      create :question, content: "q2", answer: "2"

      get "/api/questions"

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      question_contents = body.map { |question| question["content"] }
      expect(question_contents).to match_array(["q1", "q2"])
      question_answers = body.map { |question| question["answer"] }
      expect(question_answers).to match_array(["1", "2"])
    end
  end

  describe "GET /api/questions/:id" do
    it "returns the specified question" do
      create :question, content: "q1", answer: "1", id: 1

      get "/api/questions/1"

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      expect(body["content"]).to eq "q1"
      expect(body["answer"]).to eq "1"
    end
  end

  describe "POST /api/questions" do
    it "creates the specified question" do
      question = {
        content: "q1",
        answer: "1"
      }

      post "/api/questions",
        params: question.to_json,
        headers: { "Content-Type": "application/json" }

      expect(response.status).to eq 201

      body = JSON.parse(response.body)
      expect(body["content"]).to eq "q1"
      expect(body["answer"]).to eq "1"
    end
  end

  describe "PATCH /api/questions/:id" do
    it "updates the specified question" do
      create :question, content: "q1", answer: "1", id: 1

      question = {
        content: "q2"
      }

      patch "/api/questions/1",
        params: question.to_json,
        headers: { "Content-Type": "application/json" }

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      expect(body["content"]).to eq "q2"
      expect(body["answer"]).to eq "1"
    end
  end

  describe "DELETE /api/questions/:id" do
    it "deletes the specified question" do
      create :question, content: "q1", answer: "1", id: 1

      delete "/api/questions/1"

      expect(response.status).to eq 204
    end
  end

end
