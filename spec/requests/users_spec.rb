require "rails_helper"

RSpec.describe "Users", type: :request do
  describe "GET /api/users" do
    it "returns all the users" do
      create :user, name: "Donald Arthur Norman", score: 9
      create :user, name: "Stephen Gary Wozniak", score: 8

      get "/api/users"

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      user_names = body.map { |user| user["name"] }
      expect(user_names).to match_array(["Donald Arthur Norman", "Stephen Gary Wozniak"])
      user_scores = body.map { |user| user["score"] }
      expect(user_scores).to match_array([9, 8])
    end
  end

  describe "GET /api/users/:id" do
    it "returns the specified user" do
      create :user, name: "Donald Arthur Norman", score: 9, id: 1

      get "/api/users/1"

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      expect(body["name"]).to eq "Donald Arthur Norman"
      expect(body["score"]).to eq 9
    end
  end

  describe "POST /api/users" do
    it "creates the specified user" do
      user = {
        name: "Donald Arthur Norman",
        score: 9
      }

      post "/api/users",
        params: user.to_json,
        headers: { "Content-Type": "application/json" }

      expect(response.status).to eq 201

      body = JSON.parse(response.body)
      expect(body["name"]).to eq "Donald Arthur Norman"
      expect(body["score"]).to eq 9
    end
  end

  describe "PATCH /api/users/:id" do
    it "updates the specified user" do
      create :user, name: "Donald Arthur Norman", score: 9, id: 1

      user = {
        name: "Steve Jobs"
      }

      patch "/api/users/1",
        params: user.to_json,
        headers: { "Content-Type": "application/json" }

      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      expect(body["name"]).to eq "Steve Jobs"
      expect(body["score"]).to eq 9
    end
  end

  describe "DELETE /api/users/:id" do
    it "deletes the specified use" do
      create :user, name: "Donald Arthur Norman", score: 9, id: 1

      delete "/api/users/1"

      expect(response.status).to eq 204
    end
  end
end
