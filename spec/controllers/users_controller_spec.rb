require "rails_helper"

RSpec.describe UsersController, type: :controller do

  describe "GET #index" do
    it "assigns all users as @users" do
      user = create(:user)
      get :index, params: {}
      expect(assigns(:users)).to eq([user])
    end

    it "assigns all users sorted by default sort (desc) as @users" do
      user1 = create(:user, created_at: "2016-01-01 00:00:00")
      user2 = create(:user, created_at: "2016-12-31 00:00:00")
      get :index, params: {}
      expect(assigns(:users)).to eq([user2, user1])
    end

    it "assigns all users sorted by desc as @users" do
      user1 = create(:user, created_at: "2016-01-01 00:00:00")
      user2 = create(:user, created_at: "2016-12-31 00:00:00")
      get :index, params: { sort_order: "desc" }
      expect(assigns(:users)).to eq([user2, user1])
    end

    it "assigns all users sorted by asc as @users" do
      user1 = create(:user, created_at: "2016-01-01 00:00:00")
      user2 = create(:user, created_at: "2016-12-31 00:00:00")
      get :index, params: { sort_order: "asc" }
      expect(assigns(:users)).to eq([user1, user2])
    end

    it "assigns all users sorted by score as @users" do
      user1 = create(:user, score: 10)
      user2 = create(:user, score: 0)
      get :index, params: { sort_by: "score" }
      expect(assigns(:users)).to eq([user1, user2])
    end

    it "assigns limited users as @users" do
      user1 = create(:user)
      user2 = create(:user)
      get :index, params: { limit: 1 }
      expect(assigns(:users)).to eq([user1])
    end

    it "assigns skipped users as @users" do
      user1 = create(:user)
      user2 = create(:user)
      get :index, params: { offset: 1 }
      expect(assigns(:users)).to eq([user2])
    end
  end

  describe "GET #show" do
    it "assigns the requested user to @user" do
      user = create(:user)
      get :show, params: { id: user.to_param }
      expect(assigns(:user)).to eq user
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new user in the database" do
        expect {
          post :create, params: { user: attributes_for(:user) }
        }.to change(User, :count).by(1)
      end
    end

    context "with invalid attributes" do
      it "does not save the new user in the database" do
        expect {
          post :create, params: { user: attributes_for(:invalid_user) }
        }.not_to change(User, :count)
      end
    end
  end

  describe "PATCH #update" do
    before :each do
      @user = create(:user,
        name: "Stephen Gary Wozniak",
        score: 6)
    end

    context "valid attributes" do
      it "locates the requested @user" do
        patch :update, params: { id: @user.to_param, user: attributes_for(:user) }
        expect(assigns(:user)).to eq(@user)
      end

      it "changes @user's attributes" do
        patch :update, params: { id: @user.to_param,
          user: attributes_for(:user,
            name: "Donald Arthur Norman",
            score: 6)
          }
        @user.reload
        expect(@user.name).to eq("Donald Arthur Norman")
        expect(@user.score).to eq(6)
      end
    end

    context "with invalid attributes" do
      it "does not change the user's name attribute" do
        patch :update, params: { id: @user.to_param,
          user: attributes_for(:user,
            name: "Donald Arthur Norman",
            score: nil)
          }
        @user.reload
        expect(@user.name).to eq("Stephen Gary Wozniak")
        expect(@user.score).to eq(6)
      end

      it "does not change the user's score attribute" do
        patch :update, params: { id: @user.to_param,
          user: attributes_for(:user,
            name: nil,
            score: 9)
          }
        @user.reload
        expect(@user.name).to eq("Stephen Gary Wozniak")
        expect(@user.score).to eq(6)
      end
    end
  end

  describe "DELETE #destroy" do
    before :each do
      @user = create(:user)
    end

    it "deletes the user" do
      expect {
        delete :destroy, params: { id: @user.to_param }
      }.to change(User, :count).by(-1)
    end
  end
end
