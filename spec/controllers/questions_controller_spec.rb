require "rails_helper"

RSpec.describe QuestionsController, type: :controller do

  describe "GET #index" do
    it "assigns all questions as @questions" do
      question = create(:question)
      get :index, params: {}
      expect(assigns(:questions)).to eq([question])
    end
  end

  describe "GET #show" do
    it "assigns the requested questtion to @question" do
      question = create(:question)
      get :show, params: { id: question.to_param }
      expect(assigns(:question)).to eq question
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new question in the database" do
        expect {
          post :create, params: { question: attributes_for(:question) }
        }.to change(Question, :count).by(1)
      end
    end

    context "with invalid attributes" do
      it "does not save the new question in the database" do
        expect {
          post :create, params: { question: attributes_for(:invalid_question) }
        }.not_to change(Question, :count)
      end
    end

  end

  describe "PATCH #update" do
    before :each do
      @question = create(:question,
        content: "content",
        answer: "answer")
    end

    context "valid attributes" do
      it "locates the requested @question" do
        patch :update, params: { id: @question.to_param, question: attributes_for(:question) }
        expect(assigns(:question)).to eq(@question)
      end

      it "changes @question's attributes" do
        patch :update, params: { id: @question.to_param,
          question: attributes_for(:question,
            content: "content2",
            answer: "answer2")
          }
        @question.reload
        expect(@question.content).to eq("content2")
        expect(@question.answer).to eq("answer2")
      end
    end

    context "with invalid attributes" do
      it "does not change the question's content attribute" do
        patch :update, params: { id: @question.to_param,
          question: attributes_for(:question,
            content: "content2",
            answer: nil)
          }
        @question.reload
        expect(@question.content).to eq("content")
        expect(@question.answer).to eq("answer")
      end

      it "does not change the question's answer attribute" do
        patch :update, params: { id: @question.to_param,
          question: attributes_for(:question,
            content: nil,
            answer: "answer2")
          }
        @question.reload
        expect(@question.content).to eq("content")
        expect(@question.answer).to eq("answer")
      end
    end
  end

  describe "DELETE #destroy" do
    before :each do
      @question = create(:question)
    end

    it "deletes the question" do
      expect {
        delete :destroy, params: { id: @question.to_param }
      }.to change(Question, :count).by(-1)
    end
  end
end
