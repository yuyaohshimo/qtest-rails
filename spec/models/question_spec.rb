require "rails_helper"

RSpec.describe Question, type: :model do
  it "is valid with a content and answer" do
    expect(build(:question)).to be_valid
  end

  it "is invalid without a content" do
    question = build(:question, content: nil)
    question.valid?
    expect(question.errors[:content]).to include("can't be blank")
  end

  it "is invalid without a answer" do
    question = build(:question, answer: nil)
    question.valid?
    expect(question.errors[:answer]).to include("can't be blank")
  end
end
