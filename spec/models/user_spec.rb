require "rails_helper"

RSpec.describe User, type: :model do
  it "is valid with a name and score" do
    expect(build(:user)).to be_valid
  end

  it "is invalid without a name" do
    user = build(:user, name: nil)
    user.valid?
    expect(user.errors[:name]).to include("can't be blank")
  end

  it "is invalid without a score" do
    user = build(:user, score: nil)
    user.valid?
    expect(user.errors[:score]).to include("can't be blank")
  end

  it "is invalid with not numericality score" do
    user = build(:user, score: "one")
    user.valid?
    expect(user.errors[:score]).to include("is not a number")
  end

  it "is invalid with negative score" do
    user = build(:user, score: -1)
    user.valid?
    expect(user.errors[:score]).to include("must be greater than or equal to 0")
  end
end
