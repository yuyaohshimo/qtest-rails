# qtest-rails

## Requirements

ruby 2.2.2p95

## Getting Started

Install packages:

```
bundle
```

Create database:

```
bundle exec rake db:create
```

Migrate database:

```
bundle exec rake db:migrate
```

Add example data:

```
bundle exec rake questions:insert
```

Run server on `localhost:3000`:

```
bundle exec rails s
```

## Test

Migrate database:

```
bundle exec rake db:migrate RAILS_ENV=test
```

Run rspec:

```
bundle exec rspec
```

## API Spec

### `GET /api/questions`

#### Response

```
200
[
  {
    <Refer to: GET /api/questions/:id>
  }...
]
```

### `GET /api/questions/:id`

#### Response

```
200
{
  "id": <integer>,
  "content": <string>,
  "answer": <string>,
  "created_at": <ISO 8601 date>,
  "updated_at": <ISO 8601 date>
}
```

### `POST /api/questions`

#### Request

```
{
  "content": <string>,
  "answer": <string>
}
```

#### Response

```
201
{
  <Refer to: GET /api/questions/:id>
}
```

### `PATCH /api/questions/:id`

#### Request

```
{
  "content": <string>, // optional
  "answer": <string> // optional
}
```

#### Response

```
200
{
  <Refer to: GET /api/questions/:id>
}
```

### `DELETE /api/questions/:id`

#### Response

```
204
```

### `GET /api/users`

#### Request

```
{
  "sort_order": "asc|desc",
  "sort_by": "score|created_at",
  "limit": <integer>,
  "offset": <integer>
}
```

#### Response

```
200
[
  {
    <Refer to: GET /api/users/:id>
  }...
]
```

### `GET /api/users/:id`

#### Response

```
200
{
  "id": <integer>,
  "name": <string>,
  "score": <integer>,
  "created_at": <ISO 8601 date>,
  "updated_at": <ISO 8601 date>
}
```

### `POST /api/users`

#### Request

```
{
  "name": <string>,
  "score": <integer>
}
```

#### Response

```
201
{
  <Refer to: GET /api/users/:id>
}
```

### `PATCH /api/users/:id`

#### Request

```
{
  "name": <string>, // optional
  "score": <integer> // optional
}
```

#### Response

```
200
{
  <Refer to: GET /api/users/:id>
}
```

### `DELETE /api/users/:id`

#### Response

```
204
```

## Example Questions

|Content|Answer|
|----|----|
|How many vowels are there in the English alphabet?|5|
|What five letter word has six left after you take two letters away?|sixty|
|Which is stronger, Tuesday or Sunday?|sunday|
|What bird can lift the heaviest weight?|crane|
|What is a foreign ant?|important|
|What is usually black and white and red all over?|newspaper|
|What kind of clothes do lawyers wear?|suit|
