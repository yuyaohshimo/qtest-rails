namespace :questions do
  desc "inserts questions"
  task insert: :environment do
    [
      {
        content: "How many vowels are there in the English alphabet?",
        answer: "5"
      },
      {
        content: "What five letter word has six left after you take two letters away?",
        answer: "sixty"
      },
      {
        content: "Which is stronger, Tuesday or Sunday?",
        answer: "sunday"
      },
      {
        content: "What bird can lift the heaviest weight?",
        answer: "crane"
      },
      {
        content: "What is a foreign ant?",
        answer: "important"
      },
      {
        content: "What is usually black and white and red all over?",
        answer: "newspaper"
      },
      {
        content: "What kind of clothes do lawyers wear?",
        answer: "suit"
      }
    ].each do |attrs|
      Question.create(attrs)
    end
  end
end
