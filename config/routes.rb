Rails.application.routes.draw do
  scope :api do
    resources :questions, only: %i(index show create destroy)
    patch "questions/:id" => "questions#update"
    resources :users, only: %i(index show create destroy)
    patch "users/:id" => "users#update"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
