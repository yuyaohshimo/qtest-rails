class ChangeDatatypeOfQuestions < ActiveRecord::Migration[5.0]
  def change
    change_column :questions, :content, :string, null: false
    change_column :questions, :answer, :string, null: false
  end
end
