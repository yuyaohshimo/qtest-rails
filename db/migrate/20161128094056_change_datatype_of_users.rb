class ChangeDatatypeOfUsers < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :name, :string, null: false
    change_column :users, :score, :integer, null: false
  end
end
